#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define dim(a) (sizeof a/ sizeof *a)

typedef struct {
  char *name;
  int age;
} person;


double numbers[] = { 645.32, 37.40, 76.30, 5.40, -34.23, 1.11, -34.94, 23.27, 635.46, -876.22, 467.73, 62.26 };

person people[] = {
  {"Hal",       20}, {"Susann", 31}, {"Dwight", 19}, {"Kassandra",  21},
  {"Lawrence",  25}, {"Cindy",  22}, {"Cory",   27}, {"Mac",        19},
  {"Romana",    27}, {"Doretha",32}, {"Danna",  20}, {"Zara",       23},
  {"Rosalyn",   26}, {"Risa",   24}, {"Benny",  28}, {"Juan",       33},
  {"Natalie",   25}
};

int compare_doubles(const void *p1, const void *p2) {
  double d1 = *(double *)p1;
  double d2 = *(double *)p2;

  return (d1 > d2) ? 1 : (d1 < d2) ? -1 : 0;
}

int compare_persons_by_name(const void *p1, const void *p2) {
  return strcmp(((person *)p1) -> name, ((person *)p2) -> name);
}

int compare_persons_by_age(const void *p1, const void *p2) {
  return ((person *)p1) -> age - ((person *)p2) -> age;
}

int compare_persons_by_age_descending_then_by_name(const void *p1, const void *p2){
  int d = compare_persons_by_age(p2,p1);
  return (d==0) ? compare_persons_by_name(p1, p2) : d;
}

int main() {
  qsort(numbers, dim(numbers), sizeof(double), compare_doubles);
  puts("Numbers sorted ascending:");
  for(int i = 0; i < dim(numbers); ++i) printf("%lf ", numbers[i]);

  qsort(people, dim(people), sizeof(person), compare_persons_by_name);
  puts("\n\nPeople sorted by name:");
  for(int i = 0; i < dim(people); ++i) printf("(\"%s\", %d) ", people[i].name, people[i].age);

  qsort(people, dim(people), sizeof(person), compare_persons_by_age_descending_then_by_name);
  puts("\n\nPeople sorted by age descending and then by name: ");
  for(int i = 0; i < dim(people); ++i) printf("(\"%s\", %d) ", people[i].name, people[i].age);

  putchar('\n');
  return(0);
}
