module Main where

import Data.List

data Person = Person { name :: String, age :: Int } deriving (Eq, Show)

numbers = [645.32, 37.40, 76.30, 5.40, -34.23, 1.11, -34.94, 23.27, 635.46, -876.22, 467.73, 62.26]

people = [Person "Hal"       20, Person "Susann" 31, Person "Dwight" 19, Person "Kassandra"  21,
          Person "Lawrence"  25, Person "Cindy"  22, Person "Cory"   27, Person "Mac"        19,
          Person "Romana"    27, Person "Doretha"32, Person "Danna"  20, Person "Zara"       23,
          Person "Rosalyn"   26, Person "Risa"   24, Person "Benny"  28, Person "Juan"       33,
          Person "Natalie"   25]

instance Ord Person where
  compare x y = compare (age y) (age x) `thenCmp` compare (name x) (name y)
    where thenCmp EQ o2 = o2
          thenCmp o1 _  = o1

main = do putStrLn "Numbers sorted ascending:"
          print $ sort numbers

          putStrLn "\nPeople sorted by name:"
          print $ sortOn name people

          putStrLn "\nPeople sorted by age descending and then by name:"
          print $ sort people
