using System;
using System.Collections.Generic;
using System.Linq;

namespace Genericsort
{
    class Genericsort
    {
        public class Person
        {
            // Implemented Automatically
            public string Name { get; set; }
            public int Age { get; set; }
        }

        public class Numbers
        {
            // Implemented Automatically
            public float Number { get; set; }
        }

        public static void Main()
        {
            List<Numbers> lNumbers = new List<Numbers> { new Numbers() { Number = 645.32f }, new Numbers() { Number = 37.40f },
                                 new Numbers() { Number = 76.30f }, new Numbers() { Number = 5.40f },
                                 new Numbers() { Number = -34.23f }, new Numbers() { Number = 1.11f },
                                 new Numbers() { Number = -34.94f }, new Numbers() { Number = 23.37f },
                                 new Numbers() { Number = 635.46f }, new Numbers() { Number = -876.22f },
                                 new Numbers() { Number = 467.73f }, new Numbers() { Number = 62.26f }};

            List<People> lPeople = new List<People> { new People() { Name = "Hal", Age = 20 }, new People() { Name = "Susann", Age = 31 },
                                                   new People() { Name = "Dwight", Age = 19 }, new People() { Name = "Kassandra", Age = 21 },
                                                   new People() { Name = "Lawrence", Age = 25 }, new People() { Name = "Cindy", Age = 22 },
                                                   new People() { Name = "Cory", Age = 27 }, new People() { Name = "Mac", Age = 19 },
                                                   new People() { Name = "Romana", Age = 27 }, new People() { Name = "Doretha", Age = 32 },
                                                   new People() { Name = "Danna", Age = 20 }, new People() { Name = "Zara", Age = 23 },
                                                   new People() { Name = "Rosalyn", Age = 26 }, new People() { Name = "Risa", Age = 24 },
                                                   new People() { Name = "Benny", Age = 28 }, new People() { Name = "Juan", Age = 33 },
                                                   new People() { Name = "Natalie", Age = 25 }};

            Console.WriteLine("Unsorted Numbers are :");
            DisplayNumberValues(lNumbers);
            List<Numbers> lSortedNumbers = lNumbers.OrderBy(n => n.Number).ToList();
            Console.WriteLine("Sorted Numbers are :");
            DisplayNumberValues(lSortedNumbers);

            Console.WriteLine("Unsorted People are :");
            DisplayPeopleValues(lPeople);

            List<People> lSortedPeople1 = lPeople.OrderBy(n => n.Name).ToList();
            Console.WriteLine(" People alphabetically (lexicographically) sorted :");
            DisplayPeopleValues(lSortedPeople1);

            List<People> lSortedPeople2 = lPeople.OrderByDescending(a => a.Age).ThenBy(n => n.Name).ToList();
            Console.WriteLine("People sorted descending by age, where people of the same age should be sorted alphabetically(lexicographically) are :");
            DisplayPeopleValues(lSortedPeople2);

            Console.ReadLine();
        }

        public static void DisplayNumberValues(List<Numbers> num)
        {            
            for (int i = 0; i < num.Count; i++)
            {
                Console.Write(num[i].Number + ", ");
            }
            Console.WriteLine();
        }

        public static void DisplayPeopleValues(List<People> peps)
        {
            for (int i = 0; i < peps.Count; i++)
            {
                Console.WriteLine("Age : " + peps[i].Age + "\tName : " + peps[i].Name + ", ");
            }
            Console.WriteLine();
        }
    }
}