from sys import exit
from operator import itemgetter

numbers = [645.32, 37.40, 76.30, 5.40, -34.23, 1.11, -34.94, 23.27, 635.46, -876.22, 467.73, 62.26]

people = [  ("Hal",       20), ("Susann", 31), ("Dwight", 19), ("Kassandra",  21),
            ("Lawrence",  25), ("Cindy",  22), ("Cory",   27), ("Mac",        19),
            ("Romana",    27), ("Doretha",32), ("Danna",  20), ("Zara",       23),
            ("Rosalyn",   26), ("Risa",   24), ("Benny",  28), ("Juan",       33),
            ("Natalie",   25) ]

def main():
    print("Numbers sorted ascending:")
    print(sorted(numbers))

    print("\nPeople sorted by name")
    people_alphabetically = sorted(people, key=itemgetter(0))
    print(people_alphabetically);

    print("\nPeople sorted by age descending and then by name:")
    print(sorted(people_alphabetically, key=itemgetter(1), reverse = True))

    return 0

if __name__ == "__main__" : exit(main());
