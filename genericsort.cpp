#include <iostream>
#include <algorithm>
#include <vector>

struct Person {
  Person(std::string name, int age) : Name(name), Age(age) {}

  std::string Name;
  int Age;

  friend std::ostream& operator<<(std::ostream& os, const Person& p) {
    os << " ( " << p.Name << ", " << p.Age << ')';
    return os;
  }
};

std::vector<double> numbers { 645.32, 37.40, 76.30, 5.40, -34.23, 1.11, -34.94, 23.27, 635.46, -876.22, 467.73, 62.26 };

std::vector<Person> people {
  Person{"Hal",       20}, Person{"Susann", 31}, Person{"Dwight", 19}, Person{"Kassandra",  21},
  Person{"Lawrence",  25}, Person{"Cindy",  22}, Person{"Cory",   27}, Person{"Mac",        19},
  Person{"Romana",    27}, Person{"Doretha",32}, Person{"Danna",  20}, Person{"Zara",       23},
  Person{"Rosalyn",   26}, Person{"Risa",   24}, Person{"Benny",  28}, Person{"Juan",       33},
  Person{"Natalie",   25}
};

int main () {
  std::sort(std::begin(numbers), std::end(numbers));
  std::cout << "Numbers sorted ascending: \n";
  for (const int& n : numbers) std::cout << ' ' << n;
  std::cout << '\n';

  std::sort(std::begin(people), std::end(people), [](Person& a, Person& b){ return a.Name < b.Name; });
  std::cout << "\nPeople sorted by name\n";
  for (const Person& p : people) std::cout << ' ' << p;
  std::cout << '\n';

  std::sort(std::begin(people), std::end(people),
      [](Person& a, Person& b){ return (a.Age == b.Age)?(a.Name < b.Name):(a.Age > b.Age); });
  std::cout <<"\nPeople sorted by age descending and then by name: \n";
  for (const Person& p : people) std::cout << ' ' << p;
  std::cout << '\n';

  return 0;
}
